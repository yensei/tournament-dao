package py.com.yensei.tournament.dao.negocio;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import py.com.yensei.tournament.dao.commons.ReturnMessage;

abstract class GestionDAO<T> {
	
	protected Logger logger = Logger.getLogger(GestionDAO.class);
	protected Session session;
	
	public ReturnMessage create(T object){
		try {
			session = ConnectDB.newInstance().getSession();
		
			//TODO agregar try cath para evitar que transaction quede abierta en caso de error
			session.beginTransaction();
			
			session.save(object);
			
			session.getTransaction().commit();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}finally {
			session.getTransaction().rollback();
			session.close();
		}
		return getSuccessMessage();
	}
	
	
	public ReturnMessage modify(T object){
		try {
			session = ConnectDB.newInstance().getSession();
			
			session.beginTransaction();
			
			session.merge(object);
			
			session.getTransaction().commit();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}finally {
			session.getTransaction().rollback();
			session.close();
		}
		
		return getSuccessMessage();
	}
	
	public ReturnMessage delete(T object){
		try {
			session = ConnectDB.newInstance().getSession();
			
			session.beginTransaction();
			
			session.delete(object);
			
			session.getTransaction().commit();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}finally {
			session.getTransaction().rollback();
			session.close();
		}
		return getSuccessMessage();
	}
	
//	public List<T> list(Class <?> object){
	public List<T> list(){
		List<T> list = null;
		try {
			session = ConnectDB.newInstance().getSession();
			
			session.beginTransaction();
			
			String simpleName = ((Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]).getSimpleName();
			
			list = session.createQuery("SELECT t FROM "+simpleName+" t").list();
			
			session.getTransaction().commit();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}finally {
			session.getTransaction().rollback();
			session.close();
		}
		
		return list;
	}
	
	protected ReturnMessage getSuccessMessage(){
		ReturnMessage message = new ReturnMessage();
		message.setReturnCode("0");
		message.setReturnMessage("success");
		return message;
	}
	
}
