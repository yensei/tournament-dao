package py.com.yensei.tournament.dao.entidades;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import org.hibernate.annotations.ForeignKey;

@Entity
@PrimaryKeyJoinColumn(name="id_persona", referencedColumnName="id_persona")
@ForeignKey(name="fk_persona_tecnico")
public class Tecnico extends Persona {

	private static final long serialVersionUID = -8125489826934568883L;

//	@Transient
//	private Map<Cargo, Persona> ayudantes;
	
	private String Observacion;
	
	@OneToOne
	private Plantilla plantilla;

	public String getObservacion() {
		return Observacion;
	}

	public void setObservacion(String observacion) {
		Observacion = observacion;
	}

//	public Map<Cargo, Persona> getAyudantes() {
//		return ayudantes;
//	}
//
//	public void setAyudantes(Map<Cargo, Persona> ayudantes) {
//		this.ayudantes = ayudantes;
//	}

	
	
}
