package py.com.yensei.tournament.dao.util;

import java.util.Calendar;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class UtilFecha {
	public Date fecha;

	public UtilFecha() {
		this.fecha = new Date();
	}

	public static String fechaPlana(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(date);
	}

	/**
	 * Retorna en formato "dd/MM/yyyy HH:mm:ss"
	 * @param date
	 * @return
	 */
	public static String timestamp(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return format.format(date);
	}
	
	/**
	 * Retorna en formato "yyyy/MM/dd HH:mm:ss"
	 * @param date
	 * @return
	 */
	public static String timestamp2(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		return format.format(date);
	}
	
	
	public static String fechaPlana1(Date date) // para estirar de la tabla y
												// mostrar
	{
		// String retorno="";
		// retorno=+(ddd.getYear()+1900)+"-"+
		// (ddd.getMonth()+1)+"-"+ddd.getDate() ;
		// retorno=+ddd.getDate()
		// +"/"+(ddd.getMonth()+1)+"/"+(ddd.getYear()+1900);
		// return retorno;
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		return format.format(date);

	}
	
	
	
	/**
	 * Formato "yyyy-MM-dd HH:mm:ss"
	 * @param fechaString
	 * @return
	 */
	public static Date convierteTimestamp(String fechaString) {
		SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Date fecha = null;
		try {
			fecha = formateador.parse(fechaString);
		} catch (ParseException e) {
			//JOptionPane.showMessageDialog(null, "Introduzca una fecha v�lida", "ADVERTENCIA", JOptionPane.WARNING_MESSAGE, new ImageIcon("C:/eclipse/workspace/FLASH_JAVA/iconos/varios/bnk.gif"));
			System.out.println("Introduzca una fecha v�lida -convierte fecha");
		}
		return fecha;
	}

	/**
	 * Formato dd/MM/yyyy
	 * @param fechaString
	 * @return
	 */
	public static Date convierteFecha(String fechaString) {
		SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
		// System.out.print(fechaString);
		Date fecha = null;
		try {
			fecha = formateador.parse(fechaString);
		} catch (ParseException e) {
			//JOptionPane.showMessageDialog(null, "Introduzca una fecha v�lida", "ADVERTENCIA", JOptionPane.WARNING_MESSAGE, new ImageIcon("C:/eclipse/workspace/FLASH_JAVA/iconos/varios/bnk.gif"));
			System.out.println("Introduzca una fecha v�lida -convierte fecha");
		}
		return fecha;
	}
	
	/**
	 * Formato yyyy/MM/dd
	 * @param fechaString
	 * @return
	 */
	public static Date convierteFecha2(String fechaString) {
		SimpleDateFormat formateador = new SimpleDateFormat("yyyy/MM/dd");
		// System.out.print(fechaString);
		Date fecha = null;
		try {
			fecha = formateador.parse(fechaString);
		} catch (ParseException e) {
			//JOptionPane.showMessageDialog(null, "Introduzca una fecha v�lida", "ADVERTENCIA", JOptionPane.WARNING_MESSAGE, new ImageIcon("C:/eclipse/workspace/FLASH_JAVA/iconos/varios/bnk.gif"));
			System.out.println("Introduzca una fecha v�lida -convierte fecha");
		}
		return fecha;
	}

	/**
	 * Verifica si una String es una fecha con pattern dd/MM/yyyy
	 * @param tFecha
	 * @return
	 */
	public static boolean fechaCorrecta(String tFecha) {
		try {
			SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
			formatoFecha.setLenient(false);
			formatoFecha.parse(tFecha);
		} catch (ParseException e) {
			return false;
		}
		return true;

	}
	
	
	/**
	 * Verifica si una String es una fecha con pattern pasado como parametro
	 * @param tFecha
	 * @param pattern
	 * @return
	 */
	public static boolean fechaCorrecta(String tFecha,String pattern) {
		try {
			SimpleDateFormat formatoFecha = new SimpleDateFormat(pattern);
			formatoFecha.setLenient(false);
			formatoFecha.parse(tFecha);
		} catch (ParseException e) {
			return false;
		}
		return true;

	}
	
	public static int calculateAge(String fecha, String pattern){
		int age = 0;
		try {
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat formateador = new SimpleDateFormat(pattern);
			Date birthdate;
				birthdate = formateador.parse(fecha);
			cal.setTime(birthdate);
			Calendar today = Calendar.getInstance();
			
			age = today.get(Calendar.YEAR) - cal.get(Calendar.YEAR);
			if(today.get(Calendar.DAY_OF_YEAR) < cal.get(Calendar.DAY_OF_YEAR)-1){
				age--;
			}
		
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return age;
	}
	
	public static int calculateAge(Date birthdate){
		int age = 0;
		Calendar cal = Calendar.getInstance();
		cal.setTime(birthdate);
		Calendar today = Calendar.getInstance();
		
		age = today.get(Calendar.YEAR) - cal.get(Calendar.YEAR);
		if(today.get(Calendar.DAY_OF_YEAR) < cal.get(Calendar.DAY_OF_YEAR)-1){
			age--;
		}
	
		return age;
	}
	
}
