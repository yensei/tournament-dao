package py.com.yensei.tournament.dao.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ForeignKey;

@Entity
@PrimaryKeyJoinColumn(name="id_persona", referencedColumnName="id_persona")
@ForeignKey(name="fk_persona_usuario")
public class Usuario extends Persona implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6769714208309696552L;

		
	@Column(name="fecha_ingreso")
	@Temporal(TemporalType.DATE) 
	private Date fechaIngreso;
	
	@Column(name="fecha_ultimo_login")
	@Temporal(TemporalType.DATE) 
	private Date fechaUltimoLogin;
	
	@Column(nullable=false, length=15)
	private String pass;
	
	@Column(nullable=false, length=15)
	private String nick;
	
//	@OneToMany(mappedBy="usuarios")
//	private List<Torneo> torneos;//puede seguir varios torneos
//	
//	
//	private List<Prediccion> predicciones;
//	
//	private Puntuacion puntuacion;
	
	public Usuario() {
		super();
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public Date getFechaUltimoLogin() {
		return fechaUltimoLogin;
	}

	public void setFechaUltimoLogin(Date fechaUltimoLogin) {
		this.fechaUltimoLogin = fechaUltimoLogin;
	}


	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

//	public List<Torneo> getTorneos() {
//		return torneos;
//	}
//
//	public void setTorneos(List<Torneo> torneos) {
//		this.torneos = torneos;
//	}
//	
//	public List<Prediccion> getPredicciones() {
//		return predicciones;
//	}
//
//	public void setPredicciones(List<Prediccion> predicciones) {
//		this.predicciones = predicciones;
//	}
//
//	public Puntuacion getPuntuacion() {
//		return puntuacion;
//	}
//
//	public void setPuntuacion(Puntuacion puntuacion) {
//		this.puntuacion = puntuacion;
//	}

}
