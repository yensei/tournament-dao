package py.com.yensei.tournament.dao.jsonobj;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import py.com.yensei.tournament.dao.entidades.Usuario;
import py.com.yensei.tournament.dao.negocio.GestionUsuario;

public class DaoUsuario {
	
	private static GestionUsuario gestion;
	
	static {
		gestion = new GestionUsuario();
	}
	
	public static void createUser(String json){
		
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			Usuario user = mapper.readValue(json, Usuario.class);
			gestion = new GestionUsuario();
			gestion.create(user);
			
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static String getAll(){
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
			result = mapper.writeValueAsString(gestion.list());
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	

}
