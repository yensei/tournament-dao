package py.com.yensei.tournament.dao.negocio;

import java.util.Date;

import py.com.yensei.tournament.dao.entidades.Usuario;
import py.com.yensei.tournament.dao.util.UtilFecha;

public class GestionUsuario extends GestionDAO<Usuario> {
	
	public int getEdad(Usuario usuario){
		return UtilFecha.calculateAge(usuario.getFechaNacimiento());
	}
	
	
	public void updateLastLogin(Usuario usuario){
		usuario.setFechaUltimoLogin(new Date());//actualiza
		try{
			session = ConnectDB.newInstance().getSession();
			
			session.beginTransaction();
			
			session.merge(usuario);
			
			session.getTransaction().commit();
			
		}catch(RuntimeException e){
			session.getTransaction().rollback();
			logger.error(e.getMessage(), e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}finally {
			session.close();
		}
	}
	
	public void createNewUser(Usuario usuario){
		try{
			session = ConnectDB.newInstance().getSession();
			
			session.beginTransaction();
			
			usuario.setFechaIngreso(new Date());//fecha actual de creacion
			session.save(usuario);
			
			session.getTransaction().commit();
		}catch(RuntimeException e){
			session.getTransaction().rollback();
			logger.error(e.getMessage(), e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}finally {
			session.close();
		}
		
	}

}
