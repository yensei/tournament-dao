package py.com.yensei.tournament.dao.entidades;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.ForeignKey;

@Entity
public class Plantilla implements Serializable {

	private static final long serialVersionUID = 1764235782474763932L;
	
	@Id
	@SequenceGenerator(name="sec_plantilla", sequenceName="tournament.sec_plantilla", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sec_plantilla")
	private long id;
	
//	@OneToOne(mappedBy="plantilla")
//	private Equipo equipo;
	
//	@OneToMany(mappedBy="plantilla")
//	private List<Jugador> jugadores;
	
	@OneToOne(mappedBy="plantilla")
	@ForeignKey(name="fk_plantilla_tecnico")
	private Tecnico tecnico;
	
//	@ManyToOne
//	@JoinColumn(name="fk_torneo", nullable=false)
//	private Torneo torneo;
	
	public Plantilla() {
		super();
	}
//	public Equipo getEquipo() {
//		return equipo;
//	}
//	public void setEquipo(Equipo equipo) {
//		this.equipo = equipo;
//	}
//	public List<Jugador> getJugadores() {
//		return jugadores;
//	}
//	public void setJugadores(List<Jugador> jugadores) {
//		this.jugadores = jugadores;
//	}
	public Tecnico getTecnico() {
		return tecnico;
	}
	public void setTecnico(Tecnico tecnico) {
		this.tecnico = tecnico;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
}
