package py.com.yensei.tournament.dao.negocio;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class ConnectDB {
	private static Configuration configuration;
	private static ServiceRegistry registry;
	
	private static SessionFactory factory;
	private static Session session;
	
	private static ConnectDB connectDB;
	
	private ConnectDB(){
		
	}
	
	private static Session newInstanceSession() throws Exception{
		if(session==null || !session.isOpen()){
			configuration = new Configuration();
			configuration.configure();
			registry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
			factory = configuration.buildSessionFactory(registry);
			session = factory.openSession();
		}
		return session;
	}
	
	public static ConnectDB newInstance() throws Exception{
		
		if(connectDB==null){
			connectDB = new ConnectDB();	
		}
		
		if(connectDB.getSession()==null || !connectDB.getSession().isOpen()){
			session = newInstanceSession();
		}
		
		return connectDB;
	}

	public Session getSession() {
		return session;
	}

	
	public ConnectDB getConnectDB() {
		return connectDB;
	}

}
