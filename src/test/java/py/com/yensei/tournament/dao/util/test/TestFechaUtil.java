package py.com.yensei.tournament.dao.util.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import py.com.yensei.tournament.dao.util.UtilFecha;

public class TestFechaUtil {
	
	private final String FECHA = "03/12/1984";
	private final String PATTERN = "dd/MM/yyyy";

	@Test
	public void testClassExist(){
		try {
	        Class.forName("py.com.yensei.tournament.dao.util.UtilFecha");
	    } catch (ClassNotFoundException e) {
	        Assert.fail("se deberia contar con la clase UtilFecha");
	    }
	}
	
	@Test
	public void testMethodExistStringString(){
		try {
			Class[] parameters = new Class[2];
			parameters[0] = String.class;
			parameters[1] = String.class;
			UtilFecha.class.getMethod("calculateAge", parameters);
		} catch (NoSuchMethodException e) {
			Assert.fail("se deberia contar con el metodo calculateAge(String, String)");
		} 
	}
	
	@Test
	public void testMethodExistDate(){
		try {
			Class[] parameters = new Class[1];
			parameters[0] = Date.class;
			UtilFecha.class.getMethod("calculateAge", parameters);
		} catch (NoSuchMethodException e) {
			Assert.fail("se deberia contar con el metodo calculateAge(Date)");
		} 
	}
	
	@Test
	public void testGetEdadCalculateNotNegative(){
		int resut  = UtilFecha.calculateAge(FECHA, PATTERN);
		Assert.assertTrue("El resultado debe ser positivo",resut>=0);
	}
	
	@Test
	public void testGetEdadCalculateLessThan110(){
		int resut  = UtilFecha.calculateAge(FECHA, PATTERN);
		Assert.assertTrue("El resultado debe ser menor a 110",resut<110);
	}
	
	@Test
	public void testGetEdadCalculateIsCorrectStringString(){
		int edadCorrectaParaLaFechaEnviada=30;
		int resut  = UtilFecha.calculateAge(FECHA, PATTERN);
		Assert.assertTrue("El resultado debe ser "+edadCorrectaParaLaFechaEnviada+" anhos, but result was: " +resut
				,resut==edadCorrectaParaLaFechaEnviada);
	}
	
	@Test
	public void testGetEdadCalculateIsCorrectDate() throws ParseException{
		int edadCorrectaParaLaFechaEnviada=30;
		SimpleDateFormat f = new SimpleDateFormat(PATTERN);
		int resut  = UtilFecha.calculateAge(f.parse(FECHA));
		Assert.assertTrue("El resultado debe ser "+edadCorrectaParaLaFechaEnviada+" anhos, but result was: " +resut
				,resut==edadCorrectaParaLaFechaEnviada);
	}
	
	
}
