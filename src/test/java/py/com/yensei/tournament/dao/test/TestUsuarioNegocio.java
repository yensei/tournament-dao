package py.com.yensei.tournament.dao.test;


import org.junit.Before;
import org.junit.Test;

import py.com.yensei.tournament.dao.entidades.Usuario;
import py.com.yensei.tournament.dao.negocio.GestionUsuario;
import py.com.yensei.tournament.dao.util.UtilFecha;

public class TestUsuarioNegocio {
	private Usuario usuario;
	
	
	@Before
	public void init(){
		usuario = new Usuario();
		usuario.setApellido("Munoz");
		usuario.setNombre("Julio");
//		usuario.setId("1");
		usuario.setNick("yensei");
		usuario.setPass("123");
		usuario.setFechaNacimiento(UtilFecha.convierteFecha("03/12/1984"));
				
	}
	
	@Test	
	public void testCreateNewUser(){
		GestionUsuario g = new GestionUsuario();
		g.createNewUser(this.usuario);
	}
	
}
