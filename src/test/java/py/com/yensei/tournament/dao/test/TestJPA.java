package py.com.yensei.tournament.dao.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import py.com.yensei.tournament.dao.entidades.Plantilla;
import py.com.yensei.tournament.dao.entidades.Tecnico;
import py.com.yensei.tournament.dao.entidades.Usuario;

public class TestJPA {
	
	private SessionFactory sessionFactory;
	private Session session;
	private static Configuration configuration;
	
	@Before
	public void init(){
		sessionFactory = TestJPA.createSessionFactory();
		session = sessionFactory.openSession();
		configuration.addAnnotatedClass(Usuario.class);
		configuration.addAnnotatedClass(Tecnico.class);
		configuration.addAnnotatedClass(Plantilla.class);
	}
	
	@Ignore
	@Test
	public void testSession(){
		
	}
	
	@Test
	public void testCreateSchema(){
		new SchemaExport(configuration).create(true, true);
	}
	
	
	@After
	public void end(){
		if(session!=null){
			session.close();
			sessionFactory.close();
		}
	}

	
	private static SessionFactory createSessionFactory() {
		configuration = new Configuration();
	    configuration.configure();
	    ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(
	            configuration.getProperties()). buildServiceRegistry();
	    return configuration.buildSessionFactory(serviceRegistry);
	}
	
	
	
}
